#!/bin/bash

set -x -euo pipefail


. /env.sh
export HDFS_CONF_dfs_namenode_name_dir=${HDFS_CONF_dfs_namenode_name_dir:-"file:///var/lib/hadoop/dfs/data/"}

if test `ls -A ${HDFS_CONF_dfs_namenode_name_dir#"file://"}` = "" ; then
    exec hdfs --config ${HADOOP_CONF_DIR} -Ddfs.namenode.name.dir=${HDFS_CONF_dfs_namenode_name_dir} \
    namenode -format ${HADOOP_CLUNAME:-hadoop-clu}
fi

exec hdfs \
  --config ${HADOOP_CONF_DIR} \
  namenode \
  -Ddfs.namenode.name.dir=${HDFS_CONF_dfs_namenode_name_dir} \
  -fs ${CORE_CONF_fs_defaultFS}
