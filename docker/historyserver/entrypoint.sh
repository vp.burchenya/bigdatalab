#!/bin/bash

set -x -euo pipefail


. /env.sh
export YARN_CONF_timeline_store_path=${YARN_CONF_timeline_store_path:-"file:///var/lib/hadoop/yarn/timeline/"}

exec yarn --config ${HADOOP_CONF_DIR} \
  historyserver \
  -Dyarn.timeline-service.leveldb-timeline-store.path=${YARN_CONF_timeline_store_path}
