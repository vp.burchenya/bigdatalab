#!/bin/bash

set -x -euo pipefail


exec "$@"
