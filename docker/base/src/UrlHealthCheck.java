import java.net.URL ;
import java.io.IOException;
import java.net.HttpURLConnection ;
import java.net.MalformedURLException ;


class UrlHealthCheck {
    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            System.err.println("Usage: UrlHealthCheck <url>") ;
            System.exit(1) ;
        }

        String url = args[0] ;
        try {
            HttpURLConnection ua = (HttpURLConnection) new URL(url).openConnection() ;
            ua.setRequestMethod("GET") ;
            ua.setRequestProperty("User-Agent", "Java URL healthcheck") ;
            Integer status = ua.getResponseCode() ;
            if (status != 200) {
                System.err.println(
                    String.format("Bad status '%d' returned by the URL '%s'", status, url)
                ) ;
                System.exit(255) ;
            }
        } catch (MalformedURLException e) {
            System.err.println(
                String.format("Not a valid URL '%s'", url)
            ) ;
            System.exit(128) ;
        } catch (IOException e) {
            System.err.println(
                String.format("Can't read from URL '%s'", url)
            ) ;
            System.exit(255) ;
        }
    }
}
