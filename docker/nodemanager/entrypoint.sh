#!/bin/bash

set -x -euo pipefail


. /env.sh

exec yarn --config ${HADOOP_CONF_DIR} nodemanager \
  -Dyarn.resourcemanager.resource-tracker.address=hadoop-resourcemanager:8031 \
  -Dyarn.resourcemanager.address=hadoop-resourcemanager:8032 \
  -Dyarn.resourcemanager.scheduler.address=hadoop-resourcemanager:8030
