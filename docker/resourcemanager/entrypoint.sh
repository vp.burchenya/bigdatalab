#!/bin/bash

set -x -euo pipefail


. /env.sh

exec yarn \
  --config ${HADOOP_CONF_DIR} \
  resourcemanager \
  -Dyarn.log.server.url=http://hadoop-historyserver:8188/applicationhistory/logs/ \
  -Dyarn.log-aggregation-enable=false
