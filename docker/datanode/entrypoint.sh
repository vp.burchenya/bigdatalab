#!/bin/bash

set -x -euo pipefail


. /env.sh
export HDFS_CONF_dfs_datanode_data_dir=${HDFS_CONF_dfs_datanode_data_dir:-"file:///var/lib/hadoop/dfs/data/"}

exec hdfs \
  --config ${HADOOP_CONF_DIR} \
  datanode \
  -Ddfs.datanode.data.dir=${HDFS_CONF_dfs_datanode_data_dir} \
  -fs ${CORE_CONF_fs_defaultFS}
