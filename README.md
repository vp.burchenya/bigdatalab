* Big Data ecosystem:
  - Apache Flume - Java, alt for NiFi
  - Apache NiFi - Java server app with dag?, batch, streaming and gui, +kylo.
  - StreamSets Data Collector - Java, Jyhton27, batch, streaming and gui.
  - Apache Goblin - Java cli and framework with map-reduce, batch and streaming.
  - Apache AirFlow - Batch and python, dag
  - Apache Flink - Java, DAG, stream, batch
  - Ooze - Java task sched
  - Yarn res manager
  - Hue - hdfs gui + task sched
  - Apache Kylin - OLAP

* Localhost's URL-s:
  - [Data Node](http://localhost:9864/datanode.html)
  - [Name Node](http://localhost:9870/dfshealth.html#tab-overview)
  - [Node Manager](http://localhost:8042/node)
  - [Resource Manager](http://localhost:8088/cluster)
